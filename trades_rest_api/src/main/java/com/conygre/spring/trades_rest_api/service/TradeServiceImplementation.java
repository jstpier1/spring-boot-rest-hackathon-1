package com.conygre.spring.trades_rest_api.service;

import java.nio.channels.UnsupportedAddressTypeException;
import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.trades_rest_api.data.TradeRepository;
import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.TradeStatus;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImplementation implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override public Collection<Trade> getTrades() {
        return repo.findAll();
    }

    @Override
    public Collection<Trade> getTradesByTicker(String ticker) {
        return repo.findByTicker(ticker);
    };

    @Override
    public void addTrade(Trade trade) {
        repo.insert(trade);
    }

    @Override
    public void deleteTrade(Trade trade) {
        repo.delete(trade);
    }

    @Override
    public void deleteTradeById(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void updateTrade(Trade trade) throws UnsupportedOperationException {
        Trade refCheckTrade = getTradeById(trade.getId());
        if(refCheckTrade!=null){
            switch (refCheckTrade.getTradeStatus()) {
                case CREATED:
                    repo.save(trade);
                    break;
                default:
                    throw new UnsupportedOperationException("You can only change the status of a CREATED trade.");
            }
        } else{
            throw new UnsupportedOperationException("Trade does not exist.");
        }
    }

    @Override
    public Trade getTradeById(ObjectId id) {
       return repo.findById(id).get();
    }
    
    @Override
    public void cancelTrade(Trade trade) throws UnsupportedOperationException {
        try {
            trade.setTradeStatus(TradeStatus.CANCELLED);
            repo.save(trade);
        } catch (UnsupportedOperationException uoe) {
            throw new UnsupportedOperationException(uoe);
        }
    }


}