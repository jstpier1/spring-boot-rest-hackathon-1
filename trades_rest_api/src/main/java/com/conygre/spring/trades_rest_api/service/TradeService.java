package com.conygre.spring.trades_rest_api.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.TradeStatus;

import org.bson.types.ObjectId;

public interface TradeService {
    
    Trade getTradeById(ObjectId id);
    Collection<Trade> getTrades();
    Collection<Trade> getTradesByTicker(String ticker);
    void addTrade(Trade trade);
    void deleteTrade(Trade trade);
    void deleteTradeById(ObjectId id);
    void updateTrade(Trade trade) throws UnsupportedOperationException;
    void cancelTrade(Trade trade) throws UnsupportedOperationException;

}