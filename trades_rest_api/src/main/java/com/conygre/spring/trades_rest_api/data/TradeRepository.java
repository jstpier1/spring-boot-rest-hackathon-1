package com.conygre.spring.trades_rest_api.data;

import java.util.Collection;

import com.conygre.spring.trades_rest_api.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
    public Collection<Trade> findByTicker(String ticker);

}