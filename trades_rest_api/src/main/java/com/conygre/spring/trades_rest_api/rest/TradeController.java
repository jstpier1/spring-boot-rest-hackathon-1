package com.conygre.spring.trades_rest_api.rest;

import java.util.Collection;

import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trades")
@CrossOrigin
public class TradeController {
    
    @Autowired
    private TradeService service;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> findAll() {
        return service.getTrades();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/id/{id}")
	public Trade getTradeByID(@PathVariable("id") String id) {
        return service.getTradeById(new ObjectId(""+id));
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/{ticker}")
	public Collection<Trade> getTradesByTicker(@PathVariable("ticker") String ticker) {
		return service.getTradesByTicker(ticker);
	}

    @RequestMapping(method = RequestMethod.POST)
	public void addTrade(@RequestBody Trade trade) {
		service.addTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteTrade(@RequestBody Trade trade) {
        service.deleteTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteTradeById(@PathVariable("id") String id) {
        service.deleteTradeById(new ObjectId(""+id));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateTrade(@RequestBody Trade trade) {
        service.updateTrade(trade);
    }


}