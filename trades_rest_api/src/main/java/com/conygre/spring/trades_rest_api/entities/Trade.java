package com.conygre.spring.trades_rest_api.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;
    private Date dateCreated;
    private String ticker;
    private int quantity;
    private double requestedPrice;
    private TradeStatus tradeStatus;

    public Trade(String ticker, int quantity, double requestedPrice) throws IllegalArgumentException, IOException {
        this.dateCreated = new Date();
        this.tradeStatus = TradeStatus.CREATED;
        
        // Validate ticker
        URL url = new URL(String.format("https://financialmodelingprep.com/api/v3/search?query=%s&apikey=demo", ticker));
        boolean empty = true;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            for (String line; (line = reader.readLine()) != null;) {
                if(line.length() != 0) empty = false;
            }
        }

        if(empty) {
            this.ticker = ticker;
        } else {
            throw new IllegalArgumentException("Invalid ticker submitted.");
        }
       
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
    }

    public Trade() {
        this.dateCreated = new Date();
        this.tradeStatus = TradeStatus.CREATED;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeStatus getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(TradeStatus tradeStatus) throws UnsupportedOperationException {
        if(!this.tradeStatus.equals(TradeStatus.CREATED)) {
            throw new UnsupportedOperationException("You can only change the status of a CREATED trade.");
        } else {
            this.tradeStatus = tradeStatus;
        }
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Trade)) return false; 
        Trade otherTrade = (Trade) o; 
        return otherTrade.getId().equals(this.getId());
    }



    
}
