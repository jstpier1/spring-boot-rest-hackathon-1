package com.conygre.spring.trades_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradesRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradesRestApiApplication.class, args);
	}

}
