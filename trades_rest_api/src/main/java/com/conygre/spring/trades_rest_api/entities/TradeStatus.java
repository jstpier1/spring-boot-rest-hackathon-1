package com.conygre.spring.trades_rest_api.entities;

public enum TradeStatus {
    CREATED,
    PENDING,
    CANCELLED,
    REJECTED,
    FILLED,
    PARTIALLY_FILLED,
    ERROR
}