package com.conygre.spring.trades_rest_api.rest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import com.conygre.spring.trades_rest_api.entities.Trade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
public class TradeControllerTest {

    private Trade tradeTestOne;
    private Trade tradeTestTwo;

    @BeforeEach
    public void setUp() throws IllegalArgumentException, IOException {
        tradeTestOne = new Trade("C",100,10);
        tradeTestTwo = new Trade("AGGY",150,15);
    }
    
    @Autowired
    private TradeController controller;

    @Test
    public void testAddTradesByController() {
        controller.addTrade(tradeTestOne);
        controller.addTrade(tradeTestTwo);
        assertTrue(controller.getTradesByTicker(tradeTestOne.getTicker())!=null&&controller.getTradesByTicker(tradeTestTwo.getTicker())!=null);
    }

    @Test
    public void testTradesReturnedByController() {
        assertTrue(controller.findAll() != null);
        controller.findAll().forEach(m -> System.out.println(m.getTicker()));
    }

    @Test
    public void testTradesReturnedByControllerByTicker() {
        String tickerName = tradeTestTwo.getTicker();
        assertTrue(controller.getTradesByTicker(tickerName) != null);
        controller.getTradesByTicker(tickerName).forEach(m -> System.out.println(m.getTicker().equals(tickerName)));
    }

    @Test
    public void testTradesReturnedByControllerByID() {
        String tickerName = tradeTestTwo.getTicker();
        assertTrue(controller.getTradesByTicker(tickerName) != null);
        controller.getTradesByTicker(tickerName).forEach(m -> assertTrue(controller.getTradeByID(m.getId().toString())!=null));
    }

    @Test
    public void testDeleteTradesByController() {
        assertFalse(controller.findAll().isEmpty());
        controller.findAll().forEach(m -> controller.deleteTrade(m));
        assertTrue(controller.findAll().isEmpty());
    }


}