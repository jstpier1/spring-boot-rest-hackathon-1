package com.conygre.spring.trades_rest_api.data;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.time.temporal.UnsupportedTemporalTypeException;

import com.conygre.spring.trades_rest_api.entities.Trade;
import com.conygre.spring.trades_rest_api.entities.TradeStatus;
import com.conygre.spring.trades_rest_api.service.TradeServiceImplementation;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TradeRepositoryTest {

    private Trade trade;
    private final String dummyID = "5f4969d236076056a358acd8";
    private String ticker;

    @Autowired
    private TradeRepository repo;

    @Autowired
    private TradeServiceImplementation service;

    @BeforeEach
    public void createAndInsertDummyTrade() throws IllegalArgumentException, IOException {
        trade = new Trade("GOOG:A", 10, 1634.12);
        trade.setId(new ObjectId(dummyID));
        ticker = "GOOG:A";
    }

    @Test
    public void canRetrieveTradeByTicker() {
        repo.insert(trade);
        Iterable<Trade> trades = repo.findByTicker(ticker);
        boolean result = false;
        for (Trade current : trades) {
            if (current.getTicker().equals(ticker)) {
                result = true;
                break;
            }
        }
        assertTrue(result);
    }

    @Test
    public void compactDiscServiceCanReturnACatalog() {
        repo.insert(trade);
        Iterable<Trade> trades = service.getTrades();
        boolean result = false;
        for (Trade current : trades) {
            if (current.getTicker().equals(ticker)) {
                result = true;
                break;
            }
        }
        assertTrue(result);

    }

    @Test
    public void canAddTrade() {
        int prevSize = (int) repo.count();
        service.addTrade(trade);
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize + 1);
    }

    @Test
    public void canGetTradeById() {
        service.addTrade(trade);
        Trade result = service.getTradeById(new ObjectId(dummyID));
        assertTrue(result != null);
    }

    @Test
    public void canDeleteTrade() {
        service.addTrade(trade);
        int prevSize = (int) repo.count();
        service.deleteTrade(trade);
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize - 1);
    }

    @Test
    public void canDeleteTradeById() {
        // Put it in
        service.addTrade(trade);
        int prevSize = (int) repo.count();
        
        // Attempt delete
        service.deleteTradeById(new ObjectId(dummyID));
        int newSize = (int) repo.count();
        assertTrue(newSize == prevSize - 1);
    }

    @Test
    public void canUpdateTrade() {
        // Put it in
        service.addTrade(trade);

        // Get existing trade data
        Trade result = service.getTradeById(new ObjectId(dummyID));
        assertTrue(result != null);
        String oldTicker = result.getTicker();

        // Update ticker field in object
        result.setTicker("AMD");

        // Attempt to update in DB
        service.updateTrade(result);
        
        // Pull object down again by ID
        Trade updatedResult = service.getTradeById(new ObjectId(dummyID));
        assertTrue(updatedResult != null);
        String newTicker = updatedResult.getTicker();

        // Compare updated fields
        assertTrue(!oldTicker.equals(newTicker));
    }

    @Test
    public void canUpdateTradeStatus() {
        // insert into DB; default status is CREATED
        service.addTrade(trade);

        // pull and get old status
        Trade result = service.getTradeById(new ObjectId(dummyID));
        assertTrue(result != null);
        TradeStatus oldStatus = result.getTradeStatus();
    
        // attempt to update status
        TradeStatus proposedNewStatus = TradeStatus.FILLED;
        result.setTradeStatus(proposedNewStatus);
        service.updateTrade(result);

        // compare old and new status from pulled result
        Trade result2 = service.getTradeById(new ObjectId(dummyID));
        assertTrue(result2 != null);
        TradeStatus actualNewStatus = result2.getTradeStatus();

        assertTrue(!oldStatus.equals(actualNewStatus));
    }

    @Test
    public void cannotUpdatePermanentTradeStatus() throws IllegalArgumentException, IOException {
        // set status to something permanent, i.e. not CREATED, and add to DB
        TradeStatus usedStatus = TradeStatus.PENDING;
        trade.setTradeStatus(usedStatus);
        service.addTrade(trade);

        // pull and verify status is not CREATED
        Trade result = service.getTradeById(new ObjectId(dummyID));
        assertTrue(result != null);
        TradeStatus pulledStatus = result.getTradeStatus();
        assertTrue(!pulledStatus.equals(TradeStatus.CREATED));
    
        // attempt to update status -- need to create a new identical obj here with CREATED or else UOE thrown prematurely
        TradeStatus proposedNewStatus = TradeStatus.FILLED;
        trade = new Trade("GOOG:A", 10, 1634.12);
        trade.setId(new ObjectId(dummyID));
        trade.setTradeStatus(proposedNewStatus);

        // Listen for exception; pass test is UOE thrown
        assertThrows(UnsupportedOperationException.class, () -> {
            service.updateTrade(result);;
        });

       
    }

    @Test
    public void canCancelTrade() {
        // Add trade
        TradeStatus oldStatus = trade.getTradeStatus();
        service.addTrade(trade);
        service.cancelTrade(trade);

        // Grab trade
        Trade updatedResult = service.getTradeById(new ObjectId(dummyID));
        TradeStatus newStatus = updatedResult.getTradeStatus();
        
        assertTrue(!oldStatus.equals(newStatus));

    }

    @AfterEach
    public void deleteDummyTrade() {
        service.deleteTradeById(new ObjectId(dummyID));
    }


}