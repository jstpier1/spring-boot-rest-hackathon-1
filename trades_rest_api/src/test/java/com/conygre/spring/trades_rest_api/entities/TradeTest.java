package com.conygre.spring.trades_rest_api.entities;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TradeTest {


    @Test
    public void canCatchTickerError() {
        assertThrows(IllegalArgumentException.class, () -> {
            Trade test = new Trade("!!!", 22, 220.00);
        });
    }
}